﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    class Tunnel2 : MapBase
    {
        public Tunnel2()
        {
            IsBirdView = false;
            SpawnPoint = new Vector2(100, 850);
            Background = Main.txtrCh2_2;
        }



        public override void OnLoaded(Playing playScreen)
        {
            base.OnLoaded(playScreen);


        }

        public override void Update(Playing playScreen)
        {
            base.Update(playScreen);



        }
    }
}
