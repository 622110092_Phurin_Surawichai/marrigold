﻿using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    abstract class MapBase
    {
        public bool IsBirdView;
        public Vector2 SpawnPoint;

        public Texture2D Background;




        public virtual void OnLoaded(Playing playScreen)
        {
        }

        public virtual void Update(Playing playScreen)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch, Playing playScreen)
        {

        }

        public virtual void OnUnloaded(Playing playScreen)
        {
        }
    }
}
