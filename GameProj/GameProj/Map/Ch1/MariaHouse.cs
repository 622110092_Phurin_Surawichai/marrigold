﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    class MariaHouse : MapBase
    {
        public bool IsShawBanTalked = false;

        public MariaHouse()
        {
            IsBirdView = false;
            SpawnPoint = new Vector2(1200, 850);
            Background = Main.txtrMariaHouse;
        }



        public override void OnLoaded(Playing playScreen)
        {
            base.OnLoaded(playScreen);

            if (!IsShawBanTalked)
                playScreen.AddEntity(new Crowded(Lines.IntroShawBan), new Vector2(200, 840));
            else
                playScreen.AddEntity(new Crowded(null), new Vector2(200, 840));
        }

        public override void Update(Playing playScreen)
        {
            base.Update(playScreen);

            if (playScreen.Player.Bound.Center.X > 1280)
                playScreen.ChangeMap(Playing.Map_House);
        }


        public override void OnUnloaded(Playing playScreen)
        {
            base.OnUnloaded(playScreen);
        }
    }
}
