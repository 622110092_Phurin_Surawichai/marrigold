﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    class EntranceTunnel : MapBase
    {
        public EntranceTunnel()
        {
            IsBirdView = false;
            SpawnPoint = new Vector2(100, 850);
            Background = Main.txtrEntranceTunnel;
        }



        public override void OnLoaded(Playing playScreen)
        {
            base.OnLoaded(playScreen);

            playScreen.AddEntity(new Rat(), new Vector2(1180, 760));
        }

        public override void Update(Playing playScreen)
        {
            base.Update(playScreen);

            if (playScreen.Player.Bound.Center.X > 1280)
                playScreen.ChangeMap(Playing.Map_Tunnel1);
        }
    }
}
