﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    class City2 : MapBase
    {
        public bool IsShopInteract = false;
        public Rectangle ShopRect = new Rectangle(370, 270, 150, 150);


        public City2()
        {
            IsBirdView = false;
            SpawnPoint = new Vector2(100, 850);
            Background = Main.txtrCity2;
        }



        public override void OnLoaded(Playing playScreen)
        {
            base.OnLoaded(playScreen);

            playScreen.AddEntity(new Merchant(), new Vector2(380, 880));
        }

        public override void Update(Playing playScreen)
        {
            base.Update(playScreen);

            if (!IsShopInteract && ShopRect.Intersects(playScreen.Player.Bound))
            {
                playScreen.InitTalkScene(playScreen.Player, Lines.ShopKeeperTalk);
                IsShopInteract = true;
            }

            if (playScreen.Player.Bound.Center.X > 1280)
                playScreen.ChangeMap(Playing.Map_EntranceTunnel);
        }
    }
}
