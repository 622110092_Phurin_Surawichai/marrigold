﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    class House : MapBase
    {
        public bool MailBox = false;
        public bool MailBoxInteracted = false;
        public Rectangle MailBox_Rect = new Rectangle(400, 300, 200, 100);



        public House()
        {
            IsBirdView = false;
            SpawnPoint = new Vector2(100, 850);
            Background = Main.txtrHouse;
        }



        public override void OnLoaded(Playing playScreen)
        {
            base.OnLoaded(playScreen);

            //playScreen.AddEntity(new Enemy() { Tint = Color.MediumPurple }, new Vector2(500, 660));
            //playScreen.AddEntity(new Enemy_Talk() { Tint = Color.MediumSeaGreen }, new Vector2(800, 660));
        }

        public override void Update(Playing playScreen)
        {
            base.Update(playScreen);

            if (playScreen.Player.Bound.Center.X < 0)
                playScreen.ChangeMap(Playing.Map_MariaHouse);
            else if (MailBoxInteracted && playScreen.Player.Bound.Center.X > 1280)
                playScreen.ChangeMap(Playing.Map_City1);


            if (MailBox && MailBox_Rect.Intersects(playScreen.Player.Bound))
            {
                playScreen.InitTalkScene(playScreen.Player, Lines.MailBox);
                MailBox = false;
                MailBoxInteracted = true;
            }
        }

        public override void OnUnloaded(Playing playScreen)
        {
            base.OnUnloaded(playScreen);
        }
    }
}
