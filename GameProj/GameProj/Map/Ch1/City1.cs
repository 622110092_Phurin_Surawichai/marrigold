﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Map
{
    class City1 : MapBase
    {
        public City1()
        {
            IsBirdView = false;
            SpawnPoint = new Vector2(100, 850);
            Background = Main.txtrCity1;
        }



        public override void OnLoaded(Playing playScreen)
        {
            base.OnLoaded(playScreen);

            playScreen.AddEntity(new Soldier(), new Vector2(380, 880));

            if (playScreen.Follower == null)
                playScreen.AddEntity(new Follower(), new Vector2(480, 850));
        }

        public override void Update(Playing playScreen)
        {
            base.Update(playScreen);

            if ((playScreen.Follower != null && playScreen.Follower.IsFollow) && 
                playScreen.Player.Bound.Center.X > 1280)
                playScreen.ChangeMap(Playing.Map_City2);
        }
    }
}
