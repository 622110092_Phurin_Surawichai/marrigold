﻿using GameProj.Screen;
using Microsoft.Xna.Framework.Media;

namespace GameProj.Line
{
    static class Lines
    {
        #region Chap 1

        public static LineBase[] IntroLine = new LineBase[]
        {
            new LineBase("", "*เพล้ง*", null, Main.txtrHolder) { Sound = Main.SFX_glassbreak },
            new LineBase("พ่อ", "Song! แจกันนั่นพ่อต้องใช้ในการแสดงสินค้าคืนนี้นะ! *ลาก* วันนี้ไปสำนึกผิดในห้องใต้ดินซะ", null, Main.txtrHolder),
            new LineBase("", "..........", null, Main.txtrHolderB),

            new LineBase("", "*กุกกักๆ*", null, Main.txtrPrologue1),
            new LineBase("", "..........", null, Main.txtrPrologue1),
            new LineBase("", "*กุกกักๆ*", null, Main.txtrPrologue1),
            new LineBase("", "..........", null, Main.txtrPrologue1),
            new LineBase("", "*ปัง ปัง ปัง!* ", null, Main.txtrPrologue1) { Sound = Main.SFX_knockdoor },

            new LineBase("", "!!!", null, Main.txtrPrologue2) ,

            new DialogWMusic1(),
            new LineBase("Song", "เธอ...เข้ามาที่นี่ได้ยังไง", null, Main.txtrPrologue3),
            new LineBase("เด็กหญิง", "แหม ใจคอจะไม่ทำความรู้จักกันก่อนเลยรึไง ชั้น Elfin", null, Main.txtrPrologue3),
            new LineBase("Song", "...แล้วตกลงเธอเข้ามาได้ยังไงกัน", null, Main.txtrPrologue3),
            new LineBase("Elfin", "อ๊ะ ขี้โกงนี่ เลี่ยงไม่บอกชื่อด้วยคำถามงั้นหรอ ชิส์ ช่างเถอะ ก็มุดเข้ามาตามรอยแยกตรงหัวมุมกำแพงบ้านนายนั่นแหละ" + '\n' +
                         "พอดีสนใจว่าในบ้านคนรวยเขาเป็นยังไงน่ะ~ ว่าแต่นายมานั่งทำอะไรคนเดียว ในห้องมืดๆแบบนี้กันล่ะ", null, Main.txtrPrologue3),

            new LineBase("Song", "นั่นมันไม่เกี่ยวอะไรกับเธอสักหน่อย...", null, Main.txtrPrologue2),
            new LineBase("", "............ ", null, Main.txtrPrologue1),
            new LineBase("Song", "ฉันโดนทำโทษน่ะสิ", null, Main.txtrPrologue1),
            new LineBase("Elfin", "ทำโทษ? ", null, Main.txtrPrologue1),
            new LineBase("Song", "อืม เวลาพ่อโมโหเขาจะเอาฉันมาขังไว้ที่ห้องนี้แหละ", null, Main.txtrPrologue1),
            new LineBase("Elfin", "โหดร้าย...แล้วแม่เธอล่ะ? ทำไมไม่ช่วยอะไรเธอเลยล่ะ?", null, Main.txtrPrologue1),
            new LineBase("Song", "แม่เขาตายไปตั้งแต่ตอนคลอดฉันแล้วล่ะ ", null, Main.txtrPrologue1),
            new LineBase("Elfin", "เพื่อนๆล่ะ?", null, Main.txtrPrologue1),
            new LineBase("Song", "ไม่มีหรอก วันๆก็เข้าๆออกๆแต่ห้องใต้ดิน พวกที่โรงเรียนก็แค่สนใจในชื่อเสียง และทรัพย์สินของตระกูลฉันเท่านั้นแหละ", null, Main.txtrPrologue1),
            new LineBase("Elfin", "...งั้น! ชั้นนี่แหละ! ที่จะเป็นเพื่อนคนแรกจริงๆให้นายเอง~", null, Main.txtrPrologue1),
            new LineBase("", "*ตุบ...ตุบ...ตุบ*", null, Main.txtrHolderB) { Sound = Main.walk },
            new LineBase("Song", "เธอควรจะออกไปได้แล้วนะ พ่อฉันกำลังมาแล้ว", null, Main.txtrPrologue2),
            new LineBase("Elfin", "งั้นพรุ่งนี้! พรุ่งนี้ฉันจะมาเล่นที่บ้านนายอีกนะ ส่วนเวลาก็คงประมาณนี้นี่แหละ", null, Main.txtrPrologue3),
            new LineBase("Song", "Song..", null, Main.txtrHolder),
            new LineBase("Elfin", "หืม?", null, Main.txtrHolder),
            new LineBase("Song", "Song Ryang-ha นั่นคือชื่อของชั้น", null, Main.txtrHolder),

            //

            new LineBase("", "*5 ปีต่อมา*", null, Main.txtrHolderB),
            new LineBase("", "ในห้องของ Song", null, Main.txtrHolder),
            new DialogWMusic2(),
            new LineBase("Elfin", "*ฮัมเพลง* ดีจังเลยน้าวิทยุเนี่ย ถ้าเป็นบ้านชั้นนะคงซื้อไม่ไหวหรอก แค่จะซื้อเครื่องเล่นเพลงก็คงต้องกินแกลบกันเป็นเดือนแล้ว " + '\n' +" ดูคุณลุงเขาจะยังมีความเป็นพ่อเหลืออยู่บ้างนะเนี่ย ถึงยอมซื้อของฟุ่มเฟือยแบบนี้ให้น่ะ", null, Main.txtrHolder),
            new LineBase("Song", "...(ทำเป็นไม่ได้ยิน)", null, Main.txtrHolder),
            new LineBase("Elfin", "*ฮัมเพลงต่อ* ", null, Main.txtrHolder),
            new DialogWMusic3(),
            new LineBase("วิทยุ", "บ่ายวันนี้ที่เมือง xxx ถนนสาย xx โรงงานของบริษัท xxx ได้เกิดเหตุสารพิษรั่วไหลขึ้น ", null, Main.txtrHolder),
            new LineBase("Song", "นั่นมันโรงงานของบริษัทที่พ่อจัดส่งสินค้าให้หนิ?", null, Main.txtrHolder),
            new LineBase("วิทยุ", "เนื่องด้วยสารพิษตัวดังกล่าวเป็นสารระเหยชนิดระเหยช้าเป็นพิเศษทำให้การกำจัดโดยวิธีธรรมชาติเป็นไปได้ล่าช้า " + '\n' +" และยังไม่มีการวินิจฉัยผลกระทบที่เกิดขึ้นอย่างชัดหากมีการสูดดมเข้าไป แต่มีการยืนยันแล้วว่าจะทำให้เซลล์ " + '\n' +" เกิดการแบ่งตัวอย่างรวดเร็วจึงอาจก่อให้เกิดมะเร็งได้ จึงขอประกาศให้ประชาชนโดยรอบโรงงานทำการอพยพโดยด่วน  ", null, Main.txtrHolder),
            new LineBase("Elfin", "พ่อเธอจะเป็นอะไรรึเปล่านะ?... ", null, Main.txtrHolder),
            new LineBase("Song", "ช่างเขาเถอะ ป่านนี้ก็คงกำลังวุ่นวายกับการไล่เก็บสินค้าที่ยังปลอดภัยอยู่แหละ ", null, Main.txtrHolder),
            new LineBase("Elfin", "อ่ะ อืม ถ้านายว่าอย่างนั้นก็คงจะจริง ", null, Main.txtrHolder),
            new DialogWMusic4(),
            new LineBase("Elfin", "อ๊ะ นี่เพลงโปรดชั้นเลย เดี๋ยวฉันจะร้องให้ฟัง! ", null, Main.txtrHolder),
            new LineBase("Song", "เห... ", null, Main.txtrHolder),
            new LineBase("Elfin", "อย่าทำหน้าแบบนั้นสิ! ยังไม่เคยร้องให้ฟังสักหน่อย!", null, Main.txtrHolder),
            new DialogWMusic5(),
            new LineBase("", "ตอนค่ำ ในห้องของ Song", null, Main.txtrHolderB),
            new LineBase("Song", "นี่ก็มืดแล้ว เธอยังไม่กลับอีกหรอ", null, Main.txtrHolder),
            new LineBase("Elfin", "อืม...วันนี้มันมืดแล้ว ขอค้างด้วยเลยก็แล้วกัน!", null, Main.txtrHolder),
            new LineBase("Song", "ที่บ้านเธอ จะไม่เป็นห่วงเอารึไง?", null, Main.txtrHolder),
            new LineBase("Elfin", "หืม~ สนใจเรื่องของฉันด้วยรึนี่~ แต่ไม่ต้องห่วงหรอกช่วงนี้กลับไปก็อยู่คนเดียวน่ะ เหงาจะตายไป", null, Main.txtrHolder),
            new LineBase("Song", "งั้นก็ตามใจเธอละกัน", null, Main.txtrHolder),
            new LineBase("Elfin", "โอเค งั้นฝันดีนะ Song ราตรีสวัสดิ์ ", null, Main.txtrHolder),
            new LineBase("", "เช้าวันต่อมา", null, Main.txtrHolderB),
            new LineBase("Song", "...", null, Main.txtrHolder),
            new LineBase("Song", "...Elfin?", null, Main.txtrHolder),
            new LineBase("Song", "(ไม่อยู่แล้ว? คงไม่ใช่ว่าแอบไปเล่นซนในบ้านจนทำข้าวของพังหรอกนะ) ", null, Main.txtrHolder),
            new LineBase("Song", "...ออกไปเช็คดูหน่อยแล้วกัน ", null, Main.txtrHolder),
            new LineBase("", "*เสียงโวยวายจากนอกบ้าน* ", null, Main.txtrHolder),
            new LineBase("Song", "เกิดอะไรขึ้น? คงไม่ใช่ว่ายัยนั่นโดนรถเหยียบอยู่หน้าบ้านหรอกนะ?", null, Main.txtrHolder),
            new LineBase("", "", null, Main.txtrHolderB),
        };

        #region Dialog w/ Music

        class DialogWMusic1 : LineBase
        {
            public override void OnStarted(Talking talkingScreen)
            {
                MediaPlayer.Play(Main.BGM_incutscene);
            }

            public DialogWMusic1() : base("เด็กหญิง", "สนใจกันได้ซะทีนะ คุณชาย", null, Main.txtrPrologue3) { }
        }

        class DialogWMusic2 : LineBase
        {
            public override void OnStarted(Talking talkingScreen)
            {
                MediaPlayer.Stop();
                MediaPlayer.Play(Main.BGM_radiosong2);
            }

            public DialogWMusic2() : base("", "(เสียงคลอเพลงจากวิทยุ) ", null, Main.txtrHolder) { }
        }

        class DialogWMusic3 : LineBase
        {
            public override void OnStarted(Talking talkingScreen)
            {
                MediaPlayer.Stop();
                MediaPlayer.Play(Main.SFX_radionoise);
            }

            public DialogWMusic3() : base("วิทยุ", "(เพลงตัดแล้วเข้าข่าวแทน) ", null, Main.txtrHolderB) { }
        }

        class DialogWMusic4 : LineBase
        {
            public override void OnStarted(Talking talkingScreen)
            {
                MediaPlayer.Stop();
                MediaPlayer.Play(Main.BGM_radiosong1);
            }

            public DialogWMusic4() : base("วิทยุ", "(กลับมาเล่นเพลงต่อ) ", null, Main.txtrHolder) { }
        }

        class DialogWMusic5 : LineBase
        {
            public override void OnStarted(Talking talkingScreen)
            {
                MediaPlayer.Stop();
            }

            public DialogWMusic5() : base("", ".........", null, Main.txtrHolderB) { }
        }

        #endregion



        public static LineBase[] IntroShawBan = new LineBase[]
        {
            new LineBase("ชาวบ้าน1", "ให้พวกเราออกไป! เจ้าหน้าที่ประสาอะไร ไม่รู้รึไงว่ารัฐสั่งอพยพแล้ว นี่ยังจะมาห้ามอ่งห้ามออกอะไรอีก!", null, Main.txtrPrologue4),
            new LineBase("ชาวบ้าน2", "ใช่ ชั้นจะย้ายออกไปอยู่กับสามีที่นอกเมือง ปล่อยให้ชั้นออกไปเถอะนะได้โปรด!", null, Main.txtrPrologue4),
            new LineBase("เจ้าหน้าที่", "ไม่ว่าใครก็ออกไปไม่ได้ทั้งนั้น! หากยังพยายามฝ่าฝืนพวกเราจะใช้กำลัง!", null, Main.txtrPrologue4),
            new LineBase("Song", "(นี่มันแปลกๆแล้ว ทำไมเจ้าหน้าที่รัฐถึงได้มาขัดขวางประชาชน ทั้งๆที่มีคำสั่งอพยพแล้วกัน? หวังว่ายัยนั่นจะยังปลอดภัยนะ...)", null, Main.txtrPrologue4),
            new LineBase("", "", null, Main.txtrHolderB),
            new ShawBanEnd(),
        };

        #region Line End

        class ShawBanEnd : LineBase
        {
            public override void OnEnded(Talking talkingScreen)
            {
                Playing.Map_MariaHouse.IsShawBanTalked = true;
                Playing.Map_House.MailBox = true;
                talkingScreen.PlayScreen.ChangeMap(Playing.Map_House);
            }

            public ShawBanEnd() : base("Song", "...(นี่ก็ครบรอบ 4 ปีแล้ว ที่ Elfin หายตัวไป วันนั้นฉันวิ่งตามไปทั่วอย่างกับคนบ้า แต่ไม่ว่าที่ไหนๆก็ ไม่มีเบาะแสอะไรเลย ตาแก่นั่น" + '\n' + "ก็ตายไปเมื่อ 2 ปีที่แล้ว ประมาทสั่งสมสารพิษเรื่อยๆจนเป็นมะเร็งตายเอง ทั้งๆที่น่าจะมีควมสุขแท้ๆ" + '\n' + "แต่พอยัยนั่นไม่อยู่แล้ว บ้านนี้ก็ไม่ต่างอะไรกับห้องใต้ดินเลย)", null, Main.txtrHolderB)
            {
            }
        }

        #endregion



        public static LineBase[] MailBox = new LineBase[]
        {
            new LineBase("Song", " จดหมายนี่มีคนมาส่งในวันที่เกิดเรื่อง น่าจะเป็นของตาแก่ ก็ฉันไม่มีเพื่อนที่จะส่งจดหมายมาให้ล่ะนะ ...ไหนๆก็ตายไปแล้วขอดูหน่อยแล้วกัน", null, null),
            new LineBase("จดหมาย", "Meet me in the marrigold garden " + '\n' +"                                      -E", null, Main.txtrHolder),
            new LineBase("Song", "Elfin?!  แต่ในเมืองไม่มีสวน marrigold หนิ?มันต้องมีอะไรอีกสิ", null, Main.txtrHolder),

            new LineBase("Song", "...เธออยากให้ชั้นออกไปเจอเธอที่นอกเมืองสินะ Elfin", null, Main.txtrHolder),
            new LineBase("Song", "คอยก่อนนะ...", null, Main.txtrHolder),
        };

        public static LineBase[] FollowerTalk = new LineBase[]
        {
            new LineBase("ผู้หญิง", "นี่! พวกคุณไม่เห็นหรือไงเมืองนี้มันไปไม่รอดแล้วนะ จริงๆมันไม่น่าจะรอดมาถึง 4 ปีด้วยซ้ำ คิดว่ามันเป็นเพราะใครกัน?" + '\n' +" พวกเราชาวเมืองนี่ไง  ทั้งชาวบ้าน หมอยา และชั้นที่เป็นอดีตสถาปนิก ทุกคนที่อาศัยอยู่ในเมืองนี้ได้ช่วยกันทำนุบำรุง" + '\n' +"มันจนทุกวันนี้แต่ตอนนี้มันเกินกว่าจะพยุงไหวแล้ว พวกเรามีสิทธิที่จะออกไปจากที่นี่ ", null, Main.txtrHolderB),
            new LineBase("เจ้าหน้าที่", "นี่คือการเตือนครั้งสุดท้าย กลับไปอยู่ในความสงบ", null, Main.txtrHolderB),
            new LineBase("Song", "Miss...", null, null),
            new LineBase("ผู้หญิง", "(เด็ก? ไม่สิน่าจะวัยรุ่น?) อะไร...ตอนนี้ชั้นกำลังอารมณ์ไม่ดีนะ ถ้าเป็นเรื่องไร้สาระก็กลับไปซะ", null, null),
            new LineBase("Song", "คุณบอกว่าจะออกไปจากเมืองใช่ไหมครับ", null, null),
            new LineBase("ผู้หญิง", "แล้ว?", null, null),
            new LineBase("Song", "ช่วยดูนี่หน่อยสิครับ", null, null),

            new LineBase("ผู้หญิง", "นี่มัน! (แผนผังเมืองนี่ เจ้าเด็กนี่ไปได้มายังไงกัน ถ้าฉันมีแผนผังนี่ฉันก็ไม่ต้องมาเน่าตายอยู่ในเมืองนี้แล้ว" + '\n' +" แต่ถ้าเจ้าเด็กนี่เอามาให้ฉันดูก็หมายความว่า...)", null, Main.txtrHolder),
            new LineBase("Chateau","หึ เจ้าหนุ่มเธอกำลังหาคนร่วมทางสินะ ฉัน Chateau Dankworth ยินดีที่ได้ร่วมทาง " + '\n' +"(มันจะดีกับฉันมากกว่าถ้าออกไปพร้อมกับเจ้าเด็กนี่ แต่ถ้ามันคิดไม่ซื่อละก็ " + '\n' +"แค่จัดการเด็กคนเดียวไม่ยากเกินความสามารถฉันหรอก)", null, null),
            new LineBase("Song", "ผม Son Ryang-ha ยินดีที่ได้ร่วมทางเช่นกันครับ Miss Chateau", null, null),
            new LineBase("Chateau", "แต่เธอมีแค่ปืนกระบอกนั้นใช่ไหม? แค่นั้นไม่พอหรอก ตั้งแต่เกิดเหตุขึ้น พวกสัตว์ที่ไม่เป็นมะเร็งตายก็พากันกลายพันธุ์ แถมดุร้ายขึ้นด้วย " + '\n' +" กระสุนเธอได้หมดก่อนแน่ คงต้องหาอาวุธให้เธอเพิ่ม อ่าจริงสิไหนๆก็จะออกไปแล้วฉันเอาตัวอย่างสารพิษในเมืองไปให้ " + '\n' +" นักข่าวข้างนอกช่วยประกาศเรื่องที่รัฐบาลได้ก่อเรื่องเน่าๆเป็นการแก้แค้นเลยก็ดีเหมือนกัน ", null, null),
        };

        public static LineBase[] ShopKeeperTalk = new LineBase[]
        {
            new LineBase("Chateau", "เตรียมตัวเรียบร้อยแล้วนะ เราจะเดินทางกันวันนี้เลย", null, null),
        };

        #endregion

        #region Chap 2





        #endregion
    }
}