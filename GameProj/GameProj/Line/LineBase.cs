﻿using GameProj.Screen;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Line
{
    class LineBase
    {
        public string Title;
        public string Text;
        public Texture2D Image;

        public ImagePos ImagePosition;
        public enum ImagePos
        { Left, Right }

        public Texture2D BG;

        public SoundEffect Sound;



        #region Constructor

        public LineBase(string title, string text)
        {
            Title = title;
            Text = text;
            Image = null;
            ImagePosition = ImagePos.Left;
        }

        public LineBase(string title, string text, Texture2D image, Texture2D bg, ImagePos imagePos = ImagePos.Left)
        {
            Title = title;
            Text = text;
            Image = image;
            ImagePosition = imagePos;

            BG = bg;
        }

        #endregion



        public virtual void OnStarted(Talking talkingScreen)
        {
        }

        public virtual void OnEnded(Talking talkingScreen)
        {
        }
    }
}
