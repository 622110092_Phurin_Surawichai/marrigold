﻿using GameProj.Entity;
using GameProj.Line;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Screen
{
    class Talking : ScreenBase
    {
        public Playing PlayScreen;
        public Player Player;
        public EntityBase Entity;

        private float LineTextSize = 28f;

        private int CurrLineIdx = 0;
        LineBase[] TalkingLines;

        private string DisplayingLine = "";
        private int DisplayingLetterIdx = 0;

        public Rectangle FullscreenRect = new Rectangle(0, 0, 1280, 720);

        private Rectangle RectPanel = new Rectangle(60, 540, 1140, 180);
        private Vector2 TitlePos = new Vector2(100, 560);
        private Vector2 LinePos = new Vector2(100, 620);



        public Talking(Playing playScreen, Player player, EntityBase entity, params LineBase[] talkingLines)
        {
            PlayScreen = playScreen;
            TalkingLines = talkingLines;
            Player = player;
            Entity = entity;
        }



        public override void LoadContent()
        {
            if (TalkingLines[CurrLineIdx].Sound != null)
                TalkingLines[CurrLineIdx].Sound.Play();
        }



        public override void Input(KeyboardMgr keyboardMgr)
        {
            if (keyboardMgr.IsKeyPressed(Keys.Enter) || keyboardMgr.IsKeyPressed(Keys.Space))
            {
                if (CurrLineIdx >= TalkingLines.Length - 1)
                {
                    if (Entity != null)
                        Entity.CooldownTick = 150;

                    ExitScreen();
                    TalkingLines[CurrLineIdx].OnEnded(this);
                }
                else
                {
                    if (DisplayingLetterIdx < TalkingLines[CurrLineIdx].Text.Length - 1)
                    {
                        DisplayingLetterIdx = TalkingLines[CurrLineIdx].Text.Length;
                        DisplayingLine = TalkingLines[CurrLineIdx].Text;
                    }
                    else
                    {
                        DisplayingLetterIdx = 0;
                        DisplayingLine = "";

                        CurrLineIdx = Math.Min(CurrLineIdx + 1, TalkingLines.Length - 1);

                        TalkingLines[CurrLineIdx].OnStarted(this);

                        if (TalkingLines[CurrLineIdx].Sound != null)
                            TalkingLines[CurrLineIdx].Sound.Play();
                    }
                }                    
            }
        }



        public override void Update(GameTime gameTime)
        {
            if (DisplayingLetterIdx < TalkingLines[CurrLineIdx].Text.Length)
            {
                DisplayingLine += TalkingLines[CurrLineIdx].Text[DisplayingLetterIdx];
                DisplayingLetterIdx++;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (TalkingLines[CurrLineIdx].BG != null)
                spriteBatch.Draw(TalkingLines[CurrLineIdx].BG, FullscreenRect, null, Color.White);

            #region Image Calculation

            Vector2 ImagePos = Vector2.Zero;
            Vector2 ImageOrigin = Vector2.Zero;
            float ImageScale = 1;

            if (TalkingLines[CurrLineIdx].Image != null)
            {
                LineBase CurrLine = TalkingLines[CurrLineIdx];

                ImagePos = Vector2.Zero;
                ImageOrigin = new Vector2(CurrLine.Image.Width / 2f, CurrLine.Image.Height);

                switch (CurrLine.ImagePosition)
                {
                    case LineBase.ImagePos.Left: ImagePos = new Vector2(320, Main.GraphicsDevc.Viewport.Height); break;
                    case LineBase.ImagePos.Right: ImagePos = new Vector2(960, Main.GraphicsDevc.Viewport.Height); break;                
                }

                int HalfScreenHeight = (Main.GraphicsDevc.Viewport.Height / 2);
                ImageScale = (TalkingLines[CurrLineIdx].Image.Height > HalfScreenHeight) ? (HalfScreenHeight / (float)CurrLine.Image.Height) : 1;

                spriteBatch.Draw(CurrLine.Image, ImagePos, null, Color.White, 0, ImageOrigin, ImageScale, SpriteEffects.None, 0);
            }

            #endregion

            spriteBatch.Draw(Main.txtrHolder, RectPanel, null, Color.Black * 0.75f);

            spriteBatch.DrawString(Main.FontL, TalkingLines[CurrLineIdx].Title, TitlePos, Color.White, 0, Vector2.Zero, (42f/ Main.FontL.LineSpacing), SpriteEffects.None, 0);
            spriteBatch.DrawString(Main.FontL, DisplayingLine, LinePos, Color.White, 0, Vector2.Zero, (LineTextSize / Main.FontL.LineSpacing), SpriteEffects.None, 0);

            if (DisplayingLetterIdx >= TalkingLines[CurrLineIdx].Text.Length - 1)
                spriteBatch.Draw(Main.txtrHolder, new Rectangle(1120, 640, 32, 32), null, Color.White);
        }



        public override void ExitScreen()
        {
            base.ExitScreen();
            PlayScreen.IsOtherActivityOnGoing = false;
        }
    }
}
