﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Screen
{
    public abstract class ScreenBase
    {
        public bool IsDrawLowerScreen = true;

        public virtual void LoadContent()
        {
        }



        public virtual void Input(KeyboardMgr keyboardMgr)
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
        }



        public virtual void ExitScreen()
        {
            ScreenMgr.RemoveScreen(this);
        }
    }
}
