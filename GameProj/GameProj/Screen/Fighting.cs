﻿using GameProj.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace GameProj.Screen
{
    class Fighting : ScreenBase
    {
        private Playing PlayScreen;
        private Player Player;
        private EntityBase Enemy;

        #region HUD Settings

        public Vector2 LifePos = new Vector2(50, 100);
        public Vector2 EnemyLifePos = new Vector2(1120, 100);

        public float LifeIconSize = 24f;

        #endregion

        #region Title / HUD

        private Rectangle TitleRect = new Rectangle(0, 0, 1280, 100);
        private Rectangle HUDRect = new Rectangle(0, 560, 1280, 160);

        private Vector2 TitleTextPos = new Vector2(640, 70);

        private string YourTurn = "Your turn to fight.";
        private string EnemyTurn = "Enemy turn to fight.";

        #endregion

        #region Player / Enemy Display

        private Vector2 PlayerDefPos = new Vector2(320, 360);
        private Vector2 PlayerPos = new Vector2(320, 360);

        private Vector2 EnemyDefPos = new Vector2(960, 360);
        private Vector2 EnemyPos = new Vector2(960, 360);


        private string DamageText = "";
        private Vector2 DamageTextPos = Vector2.Zero;
        private int DamageTextTime = 0;

        private string RunFailedText = "Run Failed!!!";

        #endregion

        #region Logic / Selection

        private int ScreenCountdown = 60;

        private bool IsYourTurn = true;

        private bool IsPlayerAttacking = false;
        private bool IsEnemyAttacking = false;

        private int CurrIndex = 0;

        private string LblAttack = "Attack";
        private string LblUseItem = "Use Item";
        private string LblRun = "Run";

        private Vector2 LblAttackPos = new Vector2(50, 600);
        private Vector2 LblUseItemPos = new Vector2(50, 640);
        private Vector2 LblRunPos = new Vector2(50, 680);

        #endregion



        public Fighting(Playing playScreen, Player player, EntityBase enemy)
        {
            IsDrawLowerScreen = false;

            PlayScreen = playScreen;
            Player = player;
            Enemy = enemy;
        }



        public override void LoadContent()
        {
            Player.Texture.ChangeTexture(Player.Standing);
            Enemy.Texture.ChangeTexture(Enemy.Standing);
        }



        public override void Input(KeyboardMgr keyboardMgr)
        {
            if ((IsYourTurn && !IsPlayerAttacking) && (!Player.IsNoHealth() && !Enemy.IsNoHealth()))
            {
                #region Menu Selection

                if (keyboardMgr.IsKeyPressed(Keys.Up))
                    CurrIndex = Math.Max(CurrIndex - 1, 0);
                else if (keyboardMgr.IsKeyPressed(Keys.Down))
                    CurrIndex = Math.Min(CurrIndex + 1, 2);
                else if (keyboardMgr.IsKeyPressed(Keys.Enter))
                {
                    if (CurrIndex == 0)
                    {
                        IsPlayerAttacking = true;
                    }
                    else if (CurrIndex == 1)
                    {

                    }
                    else if (CurrIndex == 2)
                    {
                        if (Main.Randomer.Next(0, 3) == 0)
                        {
                            DamageText = RunFailedText;
                            DamageTextPos = PlayerDefPos - (Vector2.UnitY * 50);
                            DamageTextTime = 120;

                            IsYourTurn = false;
                            IsEnemyAttacking = true;
                        }
                        else
                        {
                            Enemy.CooldownTick = 180;
                            ExitScreen();
                        }
                    }
                }

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
            #region Attacking

            if (IsPlayerAttacking && EnemyPos.X >= EnemyDefPos.X)
            {
                PlayerPos.X += 10;
                Player.Texture.ChangeTexture(Player.Walking);
                Player.Texture.PlaySequence(Player.WalkingAnim);

                if (PlayerPos.X >= EnemyDefPos.X)
                {
                    int Damage = Main.Randomer.Next(Player.Damage, Player.Damage + 2);

                    #region Damage Text

                    DamageText = Damage.ToString();
                    DamageTextPos = EnemyDefPos - (Vector2.UnitY * Enemy.Bound.Height);
                    DamageTextTime = 60;

                    #endregion

                    Player.Texture.ChangeTexture(Player.Attack);

                    if (Player.HitSound != null)
                        Player.HitSound.Play();
                    Enemy.Hit(Damage, 0);

                    IsYourTurn = false;
                    IsPlayerAttacking = false;

                    if (!Enemy.IsNoHealth())
                        IsEnemyAttacking = true;
                }
            }
            else
            {
                PlayerPos.X = Math.Max(PlayerPos.X - 10, PlayerDefPos.X);

                #region Animation

                if (!Player.IsDying() && !Player.IsDead())
                {
                    if (Player.Texture.CurrentSwitch == Player.Attack &&
                        Player.Texture.isFinnishedAnim)
                    {
                        Player.Texture.ChangeTexture(Player.Standing);
                        Player.Texture.PlaySequence(Player.StandingAnim);
                    }
                    else Player.Texture.PlaySequence(Player.AttackAnim);
                }

                #endregion
            }


            if (IsEnemyAttacking && PlayerPos.X <= PlayerDefPos.X)
            {
                EnemyPos.X -= 10;
                Enemy.Texture.ChangeTexture(Enemy.Walking);
                Enemy.Texture.PlaySequence(Enemy.WalkingAnim);

                if (EnemyPos.X <= PlayerDefPos.X)
                {
                    int Damage = Main.Randomer.Next(Enemy.Damage, Enemy.Damage + 2);

                    #region Damage Text

                    DamageText = Damage.ToString();
                    DamageTextPos = PlayerDefPos - (Vector2.UnitY * Player.Bound.Height);
                    DamageTextTime = 60;

                    #endregion

                    Enemy.Texture.ChangeTexture(Enemy.Attack);

                    if (Enemy.HitSound != null)
                        Enemy.HitSound.Play();
                    Player.Hit(Damage, 0);

                    IsYourTurn = true;
                    IsEnemyAttacking = false;
                }
            }
            else
            {
                EnemyPos.X = Math.Min(EnemyPos.X + 10, EnemyDefPos.X);

                #region Animation

                if (!Enemy.IsDying() && !Enemy.IsDead())
                {
                    if (Enemy.Texture.CurrentSwitch == Enemy.Attack &&
                        Enemy.Texture.isFinnishedAnim)
                    {
                        Enemy.Texture.ChangeTexture(Enemy.Standing);
                        Enemy.Texture.PlaySequence(Enemy.StandingAnim);
                    }
                    else Enemy.Texture.PlaySequence(Enemy.AttackAnim);
                }

                #endregion
            }

            #endregion

            #region Damage Text

            if (DamageTextTime > 0)
                DamageTextTime = Math.Max(DamageTextTime - 1, 0);
            else
            {
                DamageText = "";
                DamageTextPos = Vector2.Zero;
            }

            #endregion

            #region Hurt Cooldown

            Player.UpdateTick();
            Enemy.UpdateTick();

            #endregion

            #region Screen Countdown

            #region Dying Animation

            if (Player.IsDying() && Player.Dying != null)
            {
                Player.Texture.ChangeTexture(Player.Dying);
                Player.Texture.PlaySequence(Player.DyingAnim);
            }
            else if (Enemy.IsDying() && Enemy.Dying != null)
            {
                Enemy.Texture.ChangeTexture(Enemy.Dying);
                Enemy.Texture.PlaySequence(Enemy.DyingAnim);
            }

            #endregion

            if (Player.IsDead() || Enemy.IsDead())
            {
                ScreenCountdown = Math.Max(ScreenCountdown - 1, 0);

                if (ScreenCountdown <= 0)
                {
                    ExitScreen();
                }
            }

            #endregion
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            SpriteFont FontS = Main.FontS;
            SpriteFont FontL = Main.FontL;

            #region Draw Background

            if (PlayScreen.CurrentMap.Background != null)
                spriteBatch.Draw(PlayScreen.CurrentMap.Background, PlayScreen.FullscreenRect, Color.White);

            #endregion

            Player.FightSceneDraw(spriteBatch, PlayerPos);
            Enemy.FightSceneDraw(spriteBatch, EnemyPos);


            #region Draw Title Rect

            spriteBatch.Draw(Main.txtrHolder, TitleRect, Color.Black * 0.75f);

            #endregion

            if (!Player.IsNoHealth() && !Enemy.IsNoHealth())
            {
                if (IsYourTurn)
                {
                    #region Title Text

                    Vector2 TitlePOri = new Vector2(FontL.MeasureString(YourTurn).X / 2f, FontL.LineSpacing / 2f);

                    float TitleScale = (40f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontL, YourTurn, TitleTextPos, Color.White, 0, TitlePOri, TitleScale, SpriteEffects.None, 0);

                    #endregion

                    if (!IsPlayerAttacking)
                    {
                        #region Draw HUD Rect

                        spriteBatch.Draw(Main.txtrHolder, HUDRect, Color.Black * 0.75f);

                        #endregion

                        #region Draw Actions

                        Vector2 AttackOri = new Vector2(0, FontL.LineSpacing / 2f);
                        Vector2 UseItemOri = new Vector2(0, FontL.LineSpacing / 2f);
                        Vector2 RunOri = new Vector2(0, FontL.LineSpacing / 2f);

                        Color AttackColor = (CurrIndex == 0) ? Color.Gold : (Color.White * 0.5f);
                        Color UseItemColor = (CurrIndex == 1) ? Color.Gold : (Color.White * 0.5f);
                        Color RunColor = (CurrIndex == 2) ? Color.Gold : (Color.White * 0.5f);

                        float ChoiceScale = (20f / FontS.LineSpacing);

                        spriteBatch.DrawString(FontL, LblAttack, LblAttackPos, AttackColor, 0, AttackOri, ChoiceScale, SpriteEffects.None, 0);
                        spriteBatch.DrawString(FontL, LblUseItem, LblUseItemPos, UseItemColor, 0, UseItemOri, ChoiceScale, SpriteEffects.None, 0);
                        spriteBatch.DrawString(FontL, LblRun, LblRunPos, RunColor, 0, RunOri, ChoiceScale, SpriteEffects.None, 0);

                        #endregion
                    }
                }
                else
                {
                    #region Title Text

                    Vector2 TitlePOri = new Vector2(FontL.MeasureString(EnemyTurn).X / 2f, FontL.LineSpacing / 2f);

                    float TitleScale = (40f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontL, EnemyTurn, TitleTextPos, Color.White, 0, TitlePOri, TitleScale, SpriteEffects.None, 0);

                    #endregion
                }
            }
            else
            {
                #region Title Text

                string FinalText = (!Player.IsNoHealth()) ? "Player Win!" : "Enemy Win!";

                Vector2 TitlePOri = new Vector2(FontL.MeasureString(FinalText).X / 2f, FontL.LineSpacing / 2f);

                float TitleScale = (40f / FontS.LineSpacing);

                spriteBatch.DrawString(FontL, FinalText, TitleTextPos, Color.White, 0, TitlePOri, TitleScale, SpriteEffects.None, 0);

                #endregion
            }

            #region Draw Health

            if (Player != null)
            {
                if (Player.Health > 0)
                {
                    Texture2D HeartIcon = Main.txtrHeart;
                    Texture2D ManaIcon = Main.txtrHolder;
                    float HeartScale = LifeIconSize / HeartIcon.Height;
                    float ManaScale = LifeIconSize / ManaIcon.Height;

                    spriteBatch.Draw(HeartIcon, new Vector2(LifePos.X, LifePos.Y), null, Color.IndianRed, 0, Vector2.Zero, HeartScale, SpriteEffects.None, 0);
                    spriteBatch.Draw(ManaIcon, new Vector2(LifePos.X, LifePos.Y + 30), null, Color.LightSteelBlue, 0, Vector2.Zero, ManaScale, SpriteEffects.None, 0);

                    float TextScale = (32f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontS, Player.Health + " / " + Player.MaxHealth, new Vector2(LifePos.X + 30, LifePos.Y), Color.IndianRed, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                    spriteBatch.DrawString(FontS, Player.Mana + " / " + Player.MaxMana, new Vector2(LifePos.X + 30, LifePos.Y + 30), Color.LightSteelBlue, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                }
                else
                {
                    string P1Text = "Player1 has died!";

                    Vector2 TextOri = new Vector2(0, FontS.LineSpacing / 2f);
                    float TextScale = (32f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontS, P1Text, LifePos, Color.White, 0, TextOri, TextScale, SpriteEffects.None, 0);
                }
            }

            if (Enemy != null)
            {
                if (Enemy.Health > 0)
                {
                    Texture2D HeartIcon = Main.txtrHeart;
                    Texture2D ManaIcon = Main.txtrHolder;
                    float HeartScale = LifeIconSize / HeartIcon.Height;
                    float ManaScale = LifeIconSize / ManaIcon.Height;

                    spriteBatch.Draw(HeartIcon, new Vector2(EnemyLifePos.X, EnemyLifePos.Y), null, Color.IndianRed, 0, Vector2.Zero, HeartScale, SpriteEffects.None, 0);
                    spriteBatch.Draw(ManaIcon, new Vector2(EnemyLifePos.X, EnemyLifePos.Y + 30), null, Color.LightSteelBlue, 0, Vector2.Zero, ManaScale, SpriteEffects.None, 0);

                    float TextScale = (32f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontS, Enemy.Health + " / " + Enemy.MaxHealth, new Vector2(EnemyLifePos.X + 30, EnemyLifePos.Y), Color.IndianRed, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                    spriteBatch.DrawString(FontS, Enemy.Mana + " / " + Enemy.MaxMana, new Vector2(EnemyLifePos.X + 30, EnemyLifePos.Y + 30), Color.LightSteelBlue, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                }
                else
                {
                    string P1Text = "Enemy has died!";

                    Vector2 TextOri = new Vector2(FontS.MeasureString(P1Text).X, FontS.LineSpacing / 2f);
                    float TextScale = (32f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontS, P1Text, EnemyLifePos, Color.White, 0, TextOri, TextScale, SpriteEffects.None, 0);
                }
            }

            #endregion

            #region Damage Text

            if (DamageTextTime > 0)
            {
                Vector2 DamageTextOri = new Vector2(FontL.MeasureString(DamageText).X / 2f, FontL.LineSpacing / 2f);
                float ChoiceScale = (24f / FontS.LineSpacing);

                spriteBatch.DrawString(FontL, DamageText, DamageTextPos, Color.White, 0, DamageTextOri, ChoiceScale, SpriteEffects.None, 0);
            }

            #endregion
        }



        public override void ExitScreen()
        {
            base.ExitScreen();
            PlayScreen.IsOtherActivityOnGoing = false;
        }
    }
}
