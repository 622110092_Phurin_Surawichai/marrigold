﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Screen
{
    class MainMenu : ScreenBase
    {
        private string LblStartText = "Start Game";
        private string LblExitText = "Exit";
        private string HintText =
            "[Up / Down] : Menu Selection      " +
            "[Enter] : Select      " +
            "[F1] : Show Controls      " +
            "[F12] : Toggle Fullscreen";

        private Rectangle FullscreenRect = new Rectangle(0, 0, 1280, 720);

        private Vector2 LogoPos = new Vector2(640, 360);
        private Vector2 LblStartPos = new Vector2(640, 520);
        private Vector2 LblExitPos = new Vector2(640, 580);
        private Vector2 LblHintPos = new Vector2(640, 700);

        private int CurrIndex = 0;

        #region Tutorial Dialog

        private bool ShowingTutorDialog = false;
        private int CurrDialogIndex = 0;

        private string TutorialTitle = "Do you want to play tutorials?.";
        private string LblYes = "Yes, Play tutorial.";
        private string LblNo = "No, Just get me to the game.";
        private string LblCancel = "Cancel";

        private Vector2 TutorTitlePos = new Vector2(640, 360);
        private Vector2 LblYesPos = new Vector2(640, 520);
        private Vector2 LblNoPos = new Vector2(640, 580);
        private Vector2 LblCancelPos = new Vector2(640, 640);

        #endregion



        public MainMenu()
        {
        }



        public override void LoadContent()
        {
            MediaPlayer.Play(Main.BGM2_Chill);
        }



        public override void Input(KeyboardMgr keyboardMgr)
        {
            if (ShowingTutorDialog)
            {
                #region Dialog Selection

                if (keyboardMgr.IsKeyPressed(Keys.Up))
                    CurrDialogIndex = Math.Max(CurrDialogIndex - 1, 0);
                else if (keyboardMgr.IsKeyPressed(Keys.Down))
                    CurrDialogIndex = Math.Min(CurrDialogIndex + 1, 2);
                else if (keyboardMgr.IsKeyPressed(Keys.Enter))
                {
                    if (CurrDialogIndex == 0)
                        ScreenMgr.SetScreen(new Playing(true));
                    else if (CurrDialogIndex == 1)
                        ScreenMgr.SetScreen(new Playing(false));
                    else if (CurrDialogIndex == 2)
                    {
                        ShowingTutorDialog = false;
                        CurrDialogIndex = 0;
                    }
                }
                else if (keyboardMgr.IsKeyPressed(Keys.Escape))
                {
                    ShowingTutorDialog = false;
                    CurrDialogIndex = 0;
                }

                #endregion
            }
            else
            {
                #region Menu Selection

                if (keyboardMgr.IsKeyPressed(Keys.Up))
                    CurrIndex = Math.Max(CurrIndex - 1, 0);
                else if (keyboardMgr.IsKeyPressed(Keys.Down))
                    CurrIndex = Math.Min(CurrIndex + 1, 1);
                else if (keyboardMgr.IsKeyPressed(Keys.Enter))
                {
                    if (CurrIndex == 0)
                        ShowingTutorDialog = true;
                    else if (CurrIndex == 1)
                        ScreenMgr.Main.Exit();
                }
                else if (keyboardMgr.IsKeyPressed(Keys.Escape))
                    CurrIndex = 1;

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            SpriteFont FontS = Main.FontS;
            SpriteFont FontL = Main.FontL;

            #region Draw Background

            Texture2D Bg = Main.txtrCover;

            spriteBatch.Draw(Bg, FullscreenRect, Color.White);

            #endregion

            #region Draw Logo

            Texture2D Logo = Main.txtrHolder;

            spriteBatch.Draw(Logo, LogoPos, null, Color.White, 0f, new Vector2(Logo.Width / 2f, Logo.Height / 2f), (60f / Logo.Height), SpriteEffects.None, 0f);

            #endregion

            #region Player Selection Text

            Vector2 OnePOri = new Vector2(FontL.MeasureString(LblStartText).X / 2f, FontL.LineSpacing / 2f);
            Vector2 TwoPOri = new Vector2(FontL.MeasureString(LblExitText).X / 2f, FontL.LineSpacing / 2f);
            Color OnePColor = (CurrIndex == 0) ? Color.Gold : (Color.White * 0.5f);
            Color TwoPColor = (CurrIndex == 1) ? Color.Gold : (Color.White * 0.5f);

            float PScale = (40f / FontL.LineSpacing);

            spriteBatch.DrawString(FontL, LblStartText, LblStartPos, OnePColor, 0, OnePOri, PScale, SpriteEffects.None, 0);
            spriteBatch.DrawString(FontL, LblExitText, LblExitPos, TwoPColor, 0, TwoPOri, PScale, SpriteEffects.None, 0);

            #endregion

            #region Hint/ Controls Text

            Vector2 HintOri = new Vector2(FontS.MeasureString(HintText).X / 2f, FontS.LineSpacing / 2f);
            Color HintColor = Color.White * 0.35f;

            float HintScale = (24f / FontS.LineSpacing);

            spriteBatch.DrawString(FontS, HintText, LblHintPos, HintColor, 0, HintOri, HintScale, SpriteEffects.None, 0);

            #endregion


            #region Tutorial Dialog

            if (ShowingTutorDialog)
            {
                #region Panel Background

                spriteBatch.Draw(Main.txtrHolder, FullscreenRect, Color.Black * 0.8f);

                #endregion

                #region Title Text

                Vector2 TitlePOri = new Vector2(FontL.MeasureString(TutorialTitle).X / 2f, FontL.LineSpacing / 2f);

                float TitleScale = (40f / FontS.LineSpacing);

                spriteBatch.DrawString(FontL, TutorialTitle, TutorTitlePos, Color.White, 0, TitlePOri, TitleScale, SpriteEffects.None, 0);

                #endregion

                #region Choice Selection Text

                Vector2 YesOri = new Vector2(FontL.MeasureString(LblYes).X / 2f, FontL.LineSpacing / 2f);
                Vector2 NoOri = new Vector2(FontL.MeasureString(LblNo).X / 2f, FontL.LineSpacing / 2f);
                Vector2 CancelOri = new Vector2(FontL.MeasureString(LblCancel).X / 2f, FontL.LineSpacing / 2f);

                Color YesColor = (CurrDialogIndex == 0) ? Color.Gold : (Color.White * 0.5f);
                Color NoColor = (CurrDialogIndex == 1) ? Color.Gold : (Color.White * 0.5f);
                Color CancelColor = (CurrDialogIndex == 2) ? Color.Gold : (Color.White * 0.5f);

                spriteBatch.DrawString(FontL, LblYes, LblStartPos, YesColor, 0, YesOri, PScale, SpriteEffects.None, 0);
                spriteBatch.DrawString(FontL, LblNo, LblExitPos, NoColor, 0, NoOri, PScale, SpriteEffects.None, 0);
                spriteBatch.DrawString(FontL, LblCancel, LblCancelPos, CancelColor, 0, CancelOri, PScale, SpriteEffects.None, 0);

                #endregion
            }

            #endregion
        }
    }
}
