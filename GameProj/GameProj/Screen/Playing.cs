﻿using GameProj.Entity;
using GameProj.Line;
using GameProj.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Screen
{
    class Playing : ScreenBase
    {
        public bool IsGameRunning = true;
        public bool IsOtherActivityOnGoing = false;

        #region HUD Settings

        public Rectangle FullscreenRect = new Rectangle(0, 0, 1280, 720);

        public Vector2 LifePos = new Vector2(50, 640);
        public float LifeIconSize = 24f;

        public Vector2 PausedTextPos = new Vector2(640, 360);

        #endregion

        #region Map

        public static MariaHouse Map_MariaHouse;
        public static House Map_House;
        public static City1 Map_City1;
        public static City2 Map_City2;
        public static EntranceTunnel Map_EntranceTunnel;

        public static Tunnel1 Map_Tunnel1;
        public static Tunnel2 Map_Tunnel2;

        #endregion

        public MapBase CurrentMap;

        public Player Player = new Player();
        public Follower Follower;

        public List<EntityBase> Entities = new List<EntityBase>();



        public Playing(bool isTutor)
        {
            if (isTutor)
                ChangeMap(new House());
            else
                ChangeMap(new House());
        }



        public override void LoadContent()
        {
            MediaPlayer.Stop();

            Map_MariaHouse = new MariaHouse();
            Map_House = new House();
            Map_City1 = new City1();
            Map_City2 = new City2();
            Map_EntranceTunnel = new EntranceTunnel();

            Map_Tunnel1 = new Tunnel1();
            Map_Tunnel2 = new Tunnel2();

            //InitTalkScene(null, Lines.IntroLine);
        }



        public override void Input(KeyboardMgr keyboardMgr)
        {
            if (keyboardMgr.IsKeyPressed(Keys.Space))
                IsGameRunning = !IsGameRunning;

            if (IsGameRunning && !IsOtherActivityOnGoing)
            {
                #region Player Control

                if (CurrentMap.IsBirdView)
                {
                    if (keyboardMgr.IsKeyDown(Keys.Up))
                        Player.MoveUp();
                    if (keyboardMgr.IsKeyDown(Keys.Down))
                        Player.MoveDown();
                    if (keyboardMgr.IsKeyDown(Keys.Left))
                        Player.MoveLeft();
                    if (keyboardMgr.IsKeyDown(Keys.Right))
                        Player.MoveRight();
                }
                else
                {
                    if (keyboardMgr.IsKeyDown(Keys.Left))
                        Player.MoveLeft();
                    if (keyboardMgr.IsKeyDown(Keys.Right))
                        Player.MoveRight();
                }

                #endregion
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (IsGameRunning && !IsOtherActivityOnGoing)
            {
                #region When Player Dead

                if (Player.IsDead())
                {
                    Player.Revive();
                    Player.Position = CurrentMap.SpawnPoint;
                }

                #endregion

                #region Update Player / Entities

                Player.Update(gameTime, this);

                if (Follower != null)
                {
                    if (Follower.IsFollow)
                    {
                        float Distance = Follower.Position.X - Player.Bound.Center.X;

                        if (Distance > 200)
                            Follower.Position.X -= Follower.Speed;
                        else if (Distance < -200)
                            Follower.Position.X += Follower.Speed;
                    }

                    Follower.Update(gameTime, this);
                }

                int EntLoopIdx = 0;

                while (EntLoopIdx < Entities.Count)
                {
                    EntityBase CurrEnt = Entities[EntLoopIdx];

                    CurrEnt.Update(gameTime, this);

                    if (CurrEnt.IsDead())
                        Entities.Remove(CurrEnt);
                    else
                    {
                        if (!CurrEnt.IsDying())
                        {
                            if (CurrEnt.CooldownTick <= 0 && CurrEnt.Bound.Intersects(Player.Bound))
                                CurrEnt.Collided(Player, this);
                        }

                        EntLoopIdx++;
                    }
                }

                #endregion

                if (CurrentMap != null)
                    CurrentMap.Update(this);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            #region Draw Background

            if (CurrentMap.Background != null)
            {
                spriteBatch.Draw(CurrentMap.Background, FullscreenRect, Color.White);
            }

            if (CurrentMap != null)
                CurrentMap.Draw(spriteBatch, this);

            #endregion

            #region Draw Player / Entities

            for (int i = 0; i < Entities.Count; i++)
            {
                EntityBase CurrEnt = Entities[i];

                CurrEnt.Draw(spriteBatch);
            }

            if (Follower != null)
            {
                Follower.Draw(spriteBatch);
            }

            Player.Draw(spriteBatch);

            #endregion

            #region Draw Health

            SpriteFont FontS = Main.FontS;

            if (Player != null)
            {
                if (Player.Health > 0)
                {
                    Texture2D HeartIcon = Main.txtrHeart;
                    Texture2D ManaIcon = Main.txtrHolder;
                    float HeartScale = LifeIconSize / HeartIcon.Height;
                    float ManaScale = LifeIconSize / ManaIcon.Height;

                    spriteBatch.Draw(HeartIcon, new Vector2(LifePos.X, LifePos.Y), null, Color.IndianRed, 0, Vector2.Zero, HeartScale, SpriteEffects.None, 0);
                    spriteBatch.Draw(ManaIcon, new Vector2(LifePos.X, LifePos.Y + 30), null, Color.LightSteelBlue, 0, Vector2.Zero, ManaScale, SpriteEffects.None, 0);

                    float TextScale = (32f / FontS.LineSpacing);

                    spriteBatch.DrawString(FontS, Player.Health + " / " + Player.MaxHealth, new Vector2(LifePos.X + 30, LifePos.Y), Color.IndianRed, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                    spriteBatch.DrawString(FontS, Player.Mana + " / " + Player.MaxMana, new Vector2(LifePos.X + 30, LifePos.Y + 30), Color.LightSteelBlue, 0, Vector2.Zero, TextScale, SpriteEffects.None, 0);
                }
                else
                {
                    string P1Text = "Player1 has died!";

                    Vector2 TextOri = new Vector2(0, 0);
                    float TextScale = (LifeIconSize / FontS.LineSpacing);

                    spriteBatch.DrawString(FontS, P1Text, LifePos, Color.Red, 0, TextOri, TextScale, SpriteEffects.None, 0);
                }
            }

            #endregion

            #region Draw Pause Layer

            if (!IsGameRunning)
            {
                spriteBatch.Draw(Main.txtrHolder, FullscreenRect, Color.Black * 0.5f);


                string P1Text = "Paused";
                Vector2 TextOri = new Vector2(FontS.MeasureString(P1Text).X / 2f, FontS.LineSpacing / 2f);
                float TextScale = (48f / FontS.LineSpacing);

                spriteBatch.DrawString(FontS, P1Text, PausedTextPos, Color.White, 0, TextOri, TextScale, SpriteEffects.None, 0);
            }

            #endregion
        }



        #region Change Map

        public void ChangeMap(MapBase mapBase)
        {
            if (CurrentMap != null)
            {
                CurrentMap.OnUnloaded(this);
                CurrentMap = null;
            }

            Entities.Clear();

            CurrentMap = mapBase;
            CurrentMap.OnLoaded(this);

            Player.Position = CurrentMap.SpawnPoint - (Vector2.UnitY * Player.Bound.Height);

            if (Follower != null)
                Follower.Position = Player.Position;
        }

        #endregion

        #region Initiate Fight Scene

        public void InitFightScene(EntityBase enemy)
        {
            if (!IsOtherActivityOnGoing)
            {
                ScreenMgr.AddScreen(new Fighting(this, Player, enemy));
                IsOtherActivityOnGoing = true;
            }
        }

        #endregion

        #region Initiate Talk Scene

        public void InitTalkScene(EntityBase entity, params LineBase[] talkingLines)
        {
            if (!IsOtherActivityOnGoing)
            {
                ScreenMgr.AddScreen(new Talking(this, Player, entity, talkingLines));
                IsOtherActivityOnGoing = true;
            }
        }

        #endregion


        #region Add Entity

        public void AddEntity(EntityBase entityBase, Vector2 position)
        {
            entityBase.Position = position - (Vector2.UnitY * entityBase.Bound.Height);
            entityBase.UpdateBound();

            Entities.Add(entityBase);
        }

        #endregion

        #region Remove Entity

        public void RemoveEntity(EntityBase entityBase)
        {
            Entities.Remove(entityBase);
        }

        #endregion
    }
}
