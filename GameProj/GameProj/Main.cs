﻿using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace GameProj
{
    public class Main : Game
    {
        #region System Fields

        public static Color ClearGraphicColor = Color.Black;
        public static Random Randomer = new Random();

        public static GraphicsDevice GraphicsDevc;
        public GraphicsDeviceManager Graphics;

        public SpriteBatch SpriteBatch;

        #endregion

        #region Loaded Assets

        public static SpriteFont FontS { get; private set; }
        public static SpriteFont FontL { get; private set; }

        public static Texture2D txtrHolder { get; private set; }
        public static Texture2D txtrHolderB { get; private set; }
        public static Texture2D txtrHeart { get; private set; }
        public static Texture2D txtrBG01 { get; private set; }

        // Sprites
        public static Texture2D txtrFMC_attack { get; private set; }
        public static Texture2D txtrFMC_battle_mode { get; private set; }
        public static Texture2D txtrFMC_hold_gun_standing { get; private set; }
        public static Texture2D txtrFMC_prepare_rifle { get; private set; }
        public static Texture2D txtrFMC_shooting { get; private set; }
        public static Texture2D txtrFMC_stand { get; private set; }
        public static Texture2D txtrFMC_walk { get; private set; }

        public static Texture2D txtrMC_attack { get; private set; }
        public static Texture2D txtrMC_battle_mode { get; private set; }
        public static Texture2D txtrMC_stand { get; private set; }
        public static Texture2D txtrMC_walk { get; private set; }

        public static Texture2D txtrMouse_idle { get; private set; }
        public static Texture2D txtrMouse_attack { get; private set; }
        public static Texture2D txtrMouse_Died { get; private set; }
        public static Texture2D txtrMouse_Defeat { get; private set; }

        public static Texture2D txtrMerchant_Idle { get; private set; }

        public static Texture2D txtrMaria_Idle { get; private set; }
        public static Texture2D txtrSoldier_Idle { get; private set; }
        public static Texture2D txtrNPC1_Idle { get; private set; }
        public static Texture2D txtrNPC2_Idle { get; private set; }
        public static Texture2D txtrNPC3_Idle { get; private set; }


        // Backgrounds (Ch1)
        public static Texture2D txtrCover { get; private set; }
        public static Texture2D txtrHouse { get; private set; }
        public static Texture2D txtrMariaHouse { get; private set; }
        public static Texture2D txtrEntranceTunnel { get; private set; }
        public static Texture2D txtrCity1 { get; private set; }
        public static Texture2D txtrCity2 { get; private set; }

        // Backgrounds (Ch2)
        public static Texture2D txtrCh2_1 { get; private set; }
        public static Texture2D txtrCh2_2 { get; private set; }

        // Prologue
        public static Texture2D txtrPrologue1 { get; private set; }
        public static Texture2D txtrPrologue2 { get; private set; }
        public static Texture2D txtrPrologue3 { get; private set; }
        public static Texture2D txtrPrologue4 { get; private set; }

        #endregion

        #region Loaded Sounds

        public static Song BGM_incutscene { get; private set; }
        public static Song BGM_radiosong1 { get; private set; }
        public static Song BGM_radiosong2 { get; private set; }
        public static Song BGM1_shopping { get; private set; }
        public static Song BGM2_Chill { get; private set; }

        public static Song SFX_radionoise { get; private set; }


        public static SoundEffect FMC_prepare_rifle { get; private set; }
        public static SoundEffect MC_attack { get; private set; }
        public static SoundEffect MC_shooting { get; private set; }
        public static SoundEffect SFX_glassbreak { get; private set; }
        public static SoundEffect SFX_knockdoor { get; private set; }
        public static SoundEffect SFX_manhit { get; private set; }
        public static SoundEffect SFX_market { get; private set; }
        public static SoundEffect SFX_ratdead { get; private set; }
        public static SoundEffect walk { get; private set; }

        #endregion



        public Main()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            Graphics.GraphicsProfile = GraphicsProfile.HiDef;
        }



        protected override void Initialize()
        {
            #region Set Graphic Resolution

            Graphics.PreferredBackBufferWidth = 1280;
            Graphics.PreferredBackBufferHeight = 720;
            Graphics.ApplyChanges();

            #endregion

            #region Initialize ScreenMgr

            ScreenMgr.Initialize(this);

            #endregion

            base.Initialize();
        }

        protected override void LoadContent()
        {
            GraphicsDevc = Graphics.GraphicsDevice;
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            #region Assets Loading

            FontS = Content.Load<SpriteFont>("FontS");
            FontL = Content.Load<SpriteFont>("FontL");

            txtrHolder = Content.Load<Texture2D>("Holder");
            txtrHolderB = Content.Load<Texture2D>("HolderB");
            txtrHeart = Content.Load<Texture2D>("Heart");
            txtrBG01 = Content.Load<Texture2D>("BG01");

            txtrFMC_attack = Content.Load<Texture2D>("Sprites/FMC_Attack");
            txtrFMC_battle_mode = Content.Load<Texture2D>("Sprites/FMC_Battle Mode");
            txtrFMC_hold_gun_standing = Content.Load<Texture2D>("Sprites/FMC_Hold Gun idle");
            txtrFMC_prepare_rifle = Content.Load<Texture2D>("Sprites/FMC_Prepare Rifle");
            txtrFMC_shooting = Content.Load<Texture2D>("Sprites/FMC_Shooting");
            txtrFMC_stand = Content.Load<Texture2D>("Sprites/FMC_Idle");
            txtrFMC_walk = Content.Load<Texture2D>("Sprites/FMC_Walking");

            txtrMC_attack = Content.Load<Texture2D>("Sprites/MC_Attack");
            txtrMC_battle_mode = Content.Load<Texture2D>("Sprites/MC_Battle mode");
            txtrMC_stand = Content.Load<Texture2D>("Sprites/MC_Standing");
            txtrMC_walk = Content.Load<Texture2D>("Sprites/MC_Walking");

            txtrMouse_idle = Content.Load<Texture2D>("Sprites/Mouse_idle");
            txtrMouse_attack = Content.Load<Texture2D>("Sprites/Mouse_attack");
            txtrMouse_Died = Content.Load<Texture2D>("Sprites/Mouse_Died");
            txtrMouse_Defeat = Content.Load<Texture2D>("Sprites/Mouse_Defeat");

            txtrMerchant_Idle = Content.Load<Texture2D>("Sprites/Merchant_idle");

            txtrMaria_Idle = Content.Load<Texture2D>("Sprites/Maria_idle");
            txtrSoldier_Idle = Content.Load<Texture2D>("Sprites/Soldier_idle");
            txtrNPC1_Idle = Content.Load<Texture2D>("Sprites/NPC1_Idle");
            txtrNPC2_Idle = Content.Load<Texture2D>("Sprites/NPC2_idle");
            txtrNPC3_Idle = Content.Load<Texture2D>("Sprites/NPC3_idle");

            txtrCover = Content.Load<Texture2D>("Cover");
            txtrHouse = Content.Load<Texture2D>("Stages/Ch1/House");
            txtrMariaHouse = Content.Load<Texture2D>("Stages/Ch1/Maria_s house (left)");
            txtrEntranceTunnel = Content.Load<Texture2D>("Stages/Ch1/Entrance Tunnel");
            txtrCity1 = Content.Load<Texture2D>("Stages/Ch1/City1 (right)");
            txtrCity2 = Content.Load<Texture2D>("Stages/Ch1/City2 (right)");

            txtrCh2_1 = Content.Load<Texture2D>("Stages/Ch2/1");
            txtrCh2_2 = Content.Load<Texture2D>("Stages/Ch2/2");

            txtrPrologue1 = Content.Load<Texture2D>("Cgs/Prologue/Prologue1");
            txtrPrologue2 = Content.Load<Texture2D>("Cgs/Prologue/Prologue2");
            txtrPrologue3 = Content.Load<Texture2D>("Cgs/Prologue/Prologue3");
            txtrPrologue4 = Content.Load<Texture2D>("Cgs/Prologue/Prologue4");

            #endregion

            #region Sound Loading

            BGM_incutscene = Content.Load<Song>("Sounds/BGM/BGM_incutscene");
            BGM_radiosong1 = Content.Load<Song>("Sounds/BGM/BGM_radiosong1");
            BGM_radiosong2 = Content.Load<Song>("Sounds/BGM/BGM_radiosong2");
            BGM1_shopping = Content.Load<Song>("Sounds/BGM/BGM1_shopping");
            BGM2_Chill = Content.Load<Song>("Sounds/BGM/BGM2_Chill");
            SFX_radionoise = Content.Load<Song>("Sounds/SFX/SFX_radionoise");

            FMC_prepare_rifle = Content.Load<SoundEffect>("Sounds/SFX/FMC_prepare_rifle");
            MC_attack = Content.Load<SoundEffect>("Sounds/SFX/MC_attack");
            MC_shooting = Content.Load<SoundEffect>("Sounds/SFX/MC_shooting");
            SFX_glassbreak = Content.Load<SoundEffect>("Sounds/SFX/SFX_glassbreak");
            SFX_knockdoor = Content.Load<SoundEffect>("Sounds/SFX/SFX_knockdoor");
            SFX_manhit = Content.Load<SoundEffect>("Sounds/SFX/SFX_manhit");
            SFX_market = Content.Load<SoundEffect>("Sounds/SFX/SFX_market");
            SFX_ratdead = Content.Load<SoundEffect>("Sounds/SFX/SFX_ratdead");
            walk = Content.Load<SoundEffect>("Sounds/SFX/walk");

            #endregion


            #region Set First Screen

            ScreenMgr.SetScreen(new MainMenu());

            #endregion
        }



        protected override void Update(GameTime gameTime)
        {
            ScreenMgr.UpdateScreen(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            #region Clear Frame

            GraphicsDevice.Clear(ClearGraphicColor);

            #endregion

            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            ScreenMgr.DrawScreen(SpriteBatch);

            SpriteBatch.End();
        }
    }



    #region Keyboard Manager

    public class KeyboardMgr
    {
        private KeyboardState CurrentKeyboardStates;
        private KeyboardState LastKeyboardStates;

        public KeyboardMgr()
        {
            CurrentKeyboardStates = new KeyboardState();
            LastKeyboardStates = new KeyboardState();
        }



        public void UpdateState()
        {
            LastKeyboardStates = CurrentKeyboardStates;
            CurrentKeyboardStates = Keyboard.GetState();
        }



        public bool IsKeyPressed(Keys key)
        { return (CurrentKeyboardStates.IsKeyDown(key) && LastKeyboardStates.IsKeyUp(key)); }

        public bool IsKeyReleased(Keys key)
        { return (CurrentKeyboardStates.IsKeyUp(key) && LastKeyboardStates.IsKeyDown(key)); }

        public bool IsKeyDown(Keys key)
        { return (CurrentKeyboardStates.IsKeyDown(key)); }
    }

    #endregion

    #region Screen Manager

    public static class ScreenMgr
    {
        public static Main Main;
        private static KeyboardMgr KeyboardMgr;
        private static List<ScreenBase> Screens;

        public static void Initialize(Main main)
        {
            Main = main;
            KeyboardMgr = new KeyboardMgr();
            Screens = new List<ScreenBase>();
        }



        public static void UpdateScreen(GameTime gameTime)
        {
            #region Update Keyboard State

            KeyboardMgr.UpdateState();

            #endregion

            #region Update Keyboard Input
            
            if (KeyboardMgr.IsKeyPressed(Keys.F12))
                Main.Graphics.ToggleFullScreen();

            #endregion

            #region Update Screen

            int ScreensCount = Screens.Count;

            if (ScreensCount > 0)
            {
                Screens[ScreensCount - 1].Input(KeyboardMgr);

                for (int i = (ScreensCount - 1); i >= 0; i--)
                {
                    if (i < Screens.Count)
                        Screens[i].Update(gameTime);
                }
            }

            #endregion
        }

        public static void DrawScreen(SpriteBatch SpriteBatch)
        {
            #region Draw Screen

            int ScreensCount = Screens.Count;

            if (ScreensCount == 1)
                Screens[ScreensCount - 1].Draw(SpriteBatch);
            else if (ScreensCount > 1)
            {
                if (!Screens[ScreensCount - 1].IsDrawLowerScreen)
                    Screens[ScreensCount - 1].Draw(SpriteBatch);
                else
                {
                    for (int i = 0; i < ScreensCount; i++)
                        Screens[i].Draw(SpriteBatch);
                }
            }

            #endregion
        }



        public static void AddScreen(ScreenBase screenBase)
        {
            Screens.Add(screenBase);

            screenBase.LoadContent();
        }

        public static void SetScreen(ScreenBase screenBase)
        {
            Screens.Clear();
            Screens.Add(screenBase);

            screenBase.LoadContent();
        }

        public static void RemoveScreen(ScreenBase screenBase)
        {
            Screens.Remove(screenBase);
        }
    }

    #endregion
}
