﻿using GameProj.Item;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Entity
{
    class Enemy_Talk : EntityBase
    {
        public Enemy_Talk()
        {
            Bound.Width = 32;
            Bound.Height = 48;

            Damage = 1;
            Health = 10;
            MaxHealth = 10;
            Mana = 10;
            MaxMana = 10;
            Speed = 2.0f;

            Texture = new TextureAtlas(Main.txtrHolder, 1, 1, 0, 0);

            HoldingItem = null;
        }



        public override void Update(GameTime gameTime, Playing playScreen)
        {
            #region Update Facing Texture
            /*
            float DistDifferX = (Bound.X - Position.X);
            float DistDifferY = (Bound.Y - Position.Y);

            if (Math.Abs(DistDifferX) > Math.Abs(DistDifferY))
            {
                if (DistDifferX < 0)
                    Texture.PlaySequence();
                else if (DistDifferX > 0)
                    Texture.PlaySequence();
            }
            else if (Math.Abs(DistDifferY) > Math.Abs(DistDifferX))
            {
                if (DistDifferY < 0)
                    Texture.PlaySequence();
                else if (DistDifferY > 0)
                    Texture.PlaySequence();
            }
            */
            #endregion

            base.Update(gameTime, playScreen);
        }

        public override void Collided(Player player, Playing playScreen)
        {
            //playScreen.InitTalkScene(this, Lines.TestTalk);
        }
    }
}
