﻿using GameProj.Item;
using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Entity
{
    class Follower : EntityBase
    {
        public bool IsFollow = false;


        public static TextureAtlasData Standing = new TextureAtlasData(Main.txtrFMC_stand, 6, 1);
        public static TextureAtlasSeq StandingAnim = new TextureAtlasSeq("S", 0, 0, 5, 15, true);

        public static TextureAtlasData Walking = new TextureAtlasData(Main.txtrFMC_walk, 4, 1);
        public static TextureAtlasSeq WalkingAnim = new TextureAtlasSeq("W", 0, 0, 3, 10, true);



        public Follower()
        {
            Bound.Width = 210;
            Bound.Height = 384;

            Damage = 2;
            Health = 10;
            MaxHealth = 10;
            Mana = 10;
            MaxMana = 10;
            Speed = 3.0f;

            Texture = new TextureAtlas(Main.txtrFMC_stand, 6, 1, 0, 0);

            HoldingItem = null;

            //SpriteEffect = SpriteEffects.FlipHorizontally;
        }



        public override void Update(GameTime gameTime, Playing playScreen)
        {
            #region Update Facing Texture
            
            float DistDifferX = (Position.X - OldPosition.X);
            float DistDifferY = (Position.Y - OldPosition.Y);

            if (Math.Abs(DistDifferX) > Math.Abs(DistDifferY))
            {
                if (DistDifferX < 0)
                {
                    SpriteEffect = SpriteEffects.FlipHorizontally;

                    Texture.ChangeTexture(Walking);
                    Texture.PlaySequence(WalkingAnim);
                }
                else if (DistDifferX > 0)
                {
                    SpriteEffect = SpriteEffects.None;

                    Texture.ChangeTexture(Walking);
                    Texture.PlaySequence(WalkingAnim);
                }
            }
            else if (Math.Abs(DistDifferY) > Math.Abs(DistDifferX))
            {
                /*
                if (DistDifferY < 0)
                    Texture.PlaySequence();
                else if (DistDifferY > 0)
                    Texture.PlaySequence();*/
            }
            else
            {
                Texture.ChangeTexture(Standing);
                Texture.PlaySequence(StandingAnim);
            }

            #endregion

            base.Update(gameTime, playScreen);
        }

        public override void Collided(Player player, Playing playScreen)
        {
            if (!IsFollow)
            {
                playScreen.InitTalkScene(this, Lines.FollowerTalk);
                IsFollow = true;

                playScreen.RemoveEntity(this);
                playScreen.Follower = this;
            }
        }
    }
}
