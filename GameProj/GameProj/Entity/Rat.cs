﻿using GameProj.Item;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Entity
{
    class Rat : EntityBase
    {
        public Rat()
        {
            Bound.Width = 128;
            Bound.Height = 128;

            Damage = 1;
            Health = 10;
            MaxHealth = 10;
            Mana = 10;
            MaxMana = 10;
            Speed = 2.0f;

            Texture = new TextureAtlas(Main.txtrMouse_idle, 5, 2, 0, 0);

            HoldingItem = null;

            DieSound = Main.SFX_ratdead;

            Standing = new TextureAtlasData(Main.txtrMouse_idle, 5, 2);
            StandingAnim = new TextureAtlasSeq("S", 0, 0, 4, 15, true);
            Walking = new TextureAtlasData(Main.txtrMouse_attack, 6, 1);
            WalkingAnim = new TextureAtlasSeq("W", 0, 0, 5, 15, true);
            Attack = new TextureAtlasData(Main.txtrMouse_attack, 6, 1);
            AttackAnim = new TextureAtlasSeq("A", 0, 0, 5, 15, false);
            Dying = new TextureAtlasData(Main.txtrMouse_Died, 4, 2);
            DyingAnim = new TextureAtlasSeq("D", 0, 0, 3, 15, false);
            Dead = new TextureAtlasData(Main.txtrMouse_Defeat, 1, 1);
            DeadAnim = new TextureAtlasSeq("D", 0, 0, 0, 1, false);
        }



        public override void Update(GameTime gameTime, Playing playScreen)
        {
            #region Update Facing Texture

            if (IsDead())
            {
                Texture.ChangeTexture(Dead);
                Texture.PlaySequence(DeadAnim);
            }
            else Texture.PlaySequence(StandingAnim);

            #endregion

            base.Update(gameTime, playScreen);
        }

        public override void Collided(Player player, Playing playScreen)
        {
            playScreen.InitFightScene(this);
        }
    }
}
