﻿using GameProj.Item;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Entity
{
    abstract class EntityBase
    {
        #region Internal Values

        // Positioning
        public Vector2 OldPosition;
        public Vector2 Position;
        public Rectangle Bound;

        // Cooldown
        public int HurtTick = 0;
        public int CooldownTick = 0;

        public SpriteEffects SpriteEffect;

        #endregion

        public int Damage;
        public int Health;
        public int MaxHealth;
        public int Mana;
        public int MaxMana;
        public float Speed;

        public TextureAtlas Texture;

        public ItemBase HoldingItem;

        public Color Tint = Color.White;

        public SoundEffect HitSound;
        public SoundEffect HurtSound;
        public SoundEffect DieSound;

        public TextureAtlasData Standing;
        public TextureAtlasSeq StandingAnim;
        public TextureAtlasData Walking;
        public TextureAtlasSeq WalkingAnim;
        public TextureAtlasData Attack;
        public TextureAtlasSeq AttackAnim;
        public TextureAtlasData Dying;
        public TextureAtlasSeq DyingAnim;
        public TextureAtlasData Dead;
        public TextureAtlasSeq DeadAnim;



        public virtual void Update(GameTime gameTime, Playing playScreen)
        {
            #region Update Facing [!!! Example !!!]
            /*
            float DistDifferX = (Bound.X - Position.X);
            float DistDifferY = (Bound.Y - Position.Y);

            if (Math.Abs(DistDifferX) > Math.Abs(DistDifferY))
            {
                if (DistDifferX < 0)
                    Texture.PlaySequence();
                else if (DistDifferX > 0)
                    Texture.PlaySequence();
            }
            else if (Math.Abs(DistDifferY) > Math.Abs(DistDifferX))
            {
                if (DistDifferY < 0)
                    Texture.PlaySequence();
                else if (DistDifferY > 0)
                    Texture.PlaySequence();
            }
            */
            #endregion

            OldPosition = Position;

            #region Update Bound

            UpdateBound();

            #endregion

            UpdateTick();
        }

        public void UpdateTick()
        {
            #region Ticker

            if (CooldownTick > 0)
                CooldownTick = Math.Max(CooldownTick - 1, 0);

            if (HurtTick > 0)
                HurtTick = Math.Max(HurtTick - 1, 0);

            #endregion
        }


        public virtual void Draw(SpriteBatch spriteBatch)
        {
            #region Draw Sprite

            Vector2 DrawOrigin = Vector2.Zero;
            Vector2 DrawScale = Vector2.One;

            #region Origin/ Scale (X)

            DrawOrigin.X = Texture.DrawOrigin.X;
            DrawScale.X = (Bound.Width / (float)Texture.SingleWidth);

            #endregion

            #region Origin/ Scale (Y)

            DrawOrigin.Y = Texture.DrawOrigin.Y;
            DrawScale.Y = (Bound.Height / (float)Texture.SingleHeight);

            #endregion

            Color Color = (HurtTick > 0) ? Color.OrangeRed : Tint;
            float ColorAlpha = (CooldownTick > 0) ? 0.75f : 1.0f;

            spriteBatch.Draw
                (Texture.Texture, Position, Texture.CurrView,
                 Color * ColorAlpha, 0, DrawOrigin, DrawScale, SpriteEffect, 0);

            #endregion
        }

        public virtual void FightSceneDraw(SpriteBatch spriteBatch, Vector2 position)
        {
            #region Draw Sprite

            Vector2 DrawOrigin = Vector2.Zero;
            Vector2 DrawScale = Vector2.One;

            #region Origin/ Scale (X)

            DrawOrigin.X = Texture.DrawOrigin.X;
            DrawScale.X = (Bound.Width / (float)Texture.SingleWidth);

            #endregion

            #region Origin/ Scale (Y)

            DrawOrigin.Y = Texture.DrawOrigin.Y;
            DrawScale.Y = (Bound.Height / (float)Texture.SingleHeight);

            #endregion

            Color Color = (HurtTick > 0) ? Color.OrangeRed : Tint;
            float ColorAlpha = (CooldownTick > 0) ? 0.75f : 1.0f;

            spriteBatch.Draw
                (Texture.Texture, position, Texture.CurrView,
                 Color * ColorAlpha, 0, DrawOrigin, DrawScale, SpriteEffects.None, 0);

            #endregion
        }



        public virtual void Collided(Player player, Playing playScreen)
        {
        }



        #region Movement

        public void MoveUp()
        { Position.Y -= Speed; }

        public void MoveDown()
        { Position.Y += Speed; }

        public void MoveLeft()
        { Position.X -= Speed; }

        public void MoveRight()
        { Position.X += Speed; }


        public void UpdateBound()
        {
            Bound.X = (int)Math.Round(Position.X) - (Bound.Width / 2);
            Bound.Y = (int)Math.Round(Position.Y) - (Bound.Height / 2);
        }

        #endregion

        #region Checker

        public bool IsNoHealth()
        {
            return (Health <= 0);
        }

        public bool IsDying()
        {
            return (Health <= 0 && CooldownTick > 0);
        }

        public bool IsDead()
        {
            return (Health <= 0 && CooldownTick <= 0);
        }

        #endregion

        #region Setter

        public bool Hit(int damage, int invincibleTick = 0)
        {
            if (Health > 0 && CooldownTick <= 0)
            {
                Health = Math.Max(Health - damage, 0);
                HurtTick = 4;

                if (HurtSound != null)
                    HurtSound.Play();

                if (Health <= 0)
                {
                    if (DieSound != null)
                        DieSound.Play();

                    CooldownTick = 90;
                    return true;
                }
                else if (invincibleTick > 0)
                    CooldownTick = invincibleTick;
            }

            return false;
        }


        public void Revive()
        {
            Health = MaxHealth;
            Mana = MaxMana;

            CooldownTick = 0;
            HurtTick = 0;
        }

        #endregion
    }



    #region Texture Atlas

    /// <summary>
    /// Texture Atlas
    /// </summary>
    public class TextureAtlas
    {
        #region Default Fields

        public bool IsVisible;

        public Texture2D Texture;
        public int ColumnCount;
        public int RowCount;
        public int SingleWidth;
        public int SingleHeight;

        public Vector2 DrawOrigin;

        #endregion

        #region View

        public Rectangle CurrView;

        private int currColumn;
        private int currRow;

        public int CurrColumn
        {
            get { return currColumn; }
            set
            {
                currColumn = MathHelper.Clamp(value, 0, ColumnCount - 1);
                CurrView.X = (SingleWidth * currColumn);
            }
        }

        public int CurrRow
        {
            get { return currRow; }
            set
            {
                currRow = MathHelper.Clamp(value, 0, RowCount - 1);
                CurrView.Y = (SingleHeight * currRow);
            }
        }

        #endregion



        #region Constructor

        public TextureAtlas(Texture2D texture, int columns, int rows, int defViewX, int defViewY)
        {
            ChangeTexture(texture, columns, rows);
            SetPosition(defViewX, defViewY);
        }

        #endregion



        #region Management

        public TextureAtlasData CurrentSwitch;

        public void ChangeTexture(TextureAtlasData dataSwitch)
        {
            if (dataSwitch.Texture != null)
            {
                if (CurrentSwitch == null || dataSwitch != CurrentSwitch)
                {
                    IsVisible = true;

                    Texture = dataSwitch.Texture;
                    ColumnCount = dataSwitch.Columns;
                    RowCount = dataSwitch.Rows;

                    CurrentSwitch = dataSwitch;

                    isFinnishedAnim = false;
                }
                else return;
            }
            else
            {
                IsVisible = false;

                Texture = Main.txtrHolder;
                ColumnCount = 1;
                RowCount = 1;
            }

            SingleWidth = (Texture.Width / ColumnCount);
            SingleHeight = (Texture.Height / RowCount);

            DrawOrigin = new Vector2(SingleWidth / 2f, SingleHeight / 2f);

            ResetView();

        }

        private void ChangeTexture(Texture2D texture, int columns, int rows)
        {
            if (texture != null)
            {
                IsVisible = true;

                Texture = texture;
                ColumnCount = columns;
                RowCount = rows;
            }
            else
            {
                IsVisible = false;

                Texture = Main.txtrHolder;
                ColumnCount = 1;
                RowCount = 1;
            }

            SingleWidth = (Texture.Width / ColumnCount);
            SingleHeight = (Texture.Height / RowCount);

            DrawOrigin = new Vector2(SingleWidth / 2f, SingleHeight / 2f);

            ResetView();

        }


        public void ResetView()
        {
            CurrView.Width = SingleWidth;
            CurrView.Height = SingleHeight;
            CurrColumn = 0;
            CurrRow = 0;
        }

        #endregion

        #region Navigate

        public void SetPosition(int toColumn, int toRow)
        {
            if (CurrColumn != toColumn)
                CurrColumn = toColumn;

            if (CurrRow != toRow)
                CurrRow = toRow;
        }

        #endregion



        #region Play Sequence

        public bool isFinnishedAnim = false;

        private TextureAtlasSeq CurrSeq;
        private int SeqTick;

        public void PlaySequence(TextureAtlasSeq newSeq, int overrideFrameTick = 0, bool isReversed = false)
        {
            if (CurrSeq.Tag != newSeq.Tag)
            {
                CurrSeq = newSeq;
                SeqTick = 0;
            }

            if (SeqTick <= 0)
            {
                switch (CurrSeq.SequenceMode)
                {
                    case TextureAtlasSeq.SequenceModes.Column: PlayColumn(CurrSeq.At, CurrSeq.From, CurrSeq.To, CurrSeq.Loop, isReversed); break;
                    case TextureAtlasSeq.SequenceModes.Row: PlayRow(CurrSeq.At, CurrSeq.From, CurrSeq.To, CurrSeq.Loop, isReversed); break;
                }

                if (overrideFrameTick > 0)
                    SeqTick = overrideFrameTick;
                else if (CurrSeq.FrameTick > 0)
                    SeqTick = CurrSeq.FrameTick;
                else
                    SeqTick = 1;
            }
            else if (CurrSeq.From - CurrSeq.To != 0)
            {
                SeqTick = Math.Max(SeqTick - 1, 0);
            }
        }

        #endregion

        #region Sequence Player

        public void PlayColumn(int atRow, int fromColumn, int toColumn, bool loop = true, bool isReversed = false)
        {
            #region Playback Setup

            int FromColumn = 0;
            int ToColumn = 0;

            if (isReversed)
            {
                FromColumn = toColumn;
                ToColumn = fromColumn;
            }
            else
            {
                FromColumn = fromColumn;
                ToColumn = toColumn;
            }

            #endregion

            if (CurrRow != atRow)
            {
                CurrRow = atRow;
                CurrColumn = FromColumn;
            }
            else
            {
                if (FromColumn < ToColumn)
                {
                    #region Play Foward

                    if (CurrColumn < ToColumn)
                        CurrColumn++;
                    else if (loop)
                        CurrColumn = FromColumn;
                    else
                        isFinnishedAnim = true;

                    #endregion
                }
                else if (FromColumn > ToColumn)
                {
                    #region Play Backward

                    if (CurrColumn > ToColumn)
                        CurrColumn--;
                    else if (loop)
                        CurrColumn = FromColumn;
                    else
                        isFinnishedAnim = true;

                    #endregion
                }
                else SetPosition(FromColumn, atRow);
            }
        }

        public void PlayRow(int atColumn, int fromRow, int toRow, bool loop = true, bool isReversed = false)
        {
            #region Playback Setup

            int FromRow = 0;
            int ToRow = 0;

            if (isReversed)
            {
                FromRow = toRow;
                ToRow = fromRow;
            }
            else
            {
                FromRow = fromRow;
                ToRow = toRow;
            }

            #endregion

            if (CurrColumn != atColumn)
            {
                CurrColumn = atColumn;
                CurrRow = FromRow;
            }
            else
            {
                if (FromRow < ToRow)
                {
                    #region Play Foward

                    if (CurrRow < ToRow)
                        CurrRow++;
                    else if (loop)
                        CurrRow = FromRow;
                    else
                        isFinnishedAnim = true;

                    #endregion
                }
                else if (FromRow > ToRow)
                {
                    #region Play Backward

                    if (CurrRow > ToRow)
                        CurrRow--;
                    else if (loop)
                        CurrRow = FromRow;
                    else
                        isFinnishedAnim = true;

                    #endregion
                }
                else SetPosition(atColumn, FromRow);
            }
        }

        #endregion
    }

    #endregion

    #region Texture Atlas Sequencer

    /// <summary>
    /// Texture Atlas Sequencer
    /// </summary>
    public struct TextureAtlasSeq
    {
        public string Tag;

        public int At;
        public int From;
        public int To;
        public int FrameTick;
        public bool Loop;

        public SequenceModes SequenceMode;
        public enum SequenceModes
        {
            Column,
            Row
        }



        public TextureAtlasSeq(string tag, int atRow, int atColumn)
        {
            Tag = tag;

            At = atRow;
            From = atColumn;
            To = atColumn;
            FrameTick = 1;
            Loop = false;
            SequenceMode = SequenceModes.Column;
        }

        public TextureAtlasSeq(string tag, int at, int from, int to, int frameTick = 1, bool loop = true, SequenceModes sequenceMode = SequenceModes.Column)
        {
            Tag = tag;

            At = at;
            From = from;
            To = to;
            FrameTick = frameTick;
            Loop = loop;
            SequenceMode = sequenceMode;
        }
    }

    #endregion

    #region Texture Data

    public class TextureAtlasData
    {
        public Texture2D Texture;
        public int Columns;
        public int Rows;

        public TextureAtlasData(Texture2D texture, int columns, int rows)
        {
            Texture = texture;
            Columns = columns;
            Rows = rows;
        }
    }

    #endregion
}
