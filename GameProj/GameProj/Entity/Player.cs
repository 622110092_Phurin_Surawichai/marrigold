﻿using GameProj.Item;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameProj.Entity
{
    class Player : EntityBase
    {
        public Player()
        {
            Bound.Width = 210;
            Bound.Height = 384;

            Damage = 2;
            Health = 10;
            MaxHealth = 10;
            Mana = 10;
            MaxMana = 10;
            Speed = 4.0f;

            Texture = new TextureAtlas(Main.txtrMC_stand, 2, 1, 0, 0);

            HoldingItem = null;

            HitSound = Main.MC_attack;
            HurtSound = Main.SFX_manhit;

            Standing = new TextureAtlasData(Main.txtrMC_stand, 2, 1);
            StandingAnim = new TextureAtlasSeq("S", 0, 0, 1, 15, true);
            Walking = new TextureAtlasData(Main.txtrMC_walk, 4, 1);
            WalkingAnim = new TextureAtlasSeq("W", 0, 0, 3, 10, true);
            Attack = new TextureAtlasData(Main.txtrMC_attack, 4, 2);
            AttackAnim = new TextureAtlasSeq("A", 0, 0, 3, 5, false);
        }



        public override void Update(GameTime gameTime, Playing playScreen)
        {
            #region Update Facing Texture
            
            float DistDifferX = (Position.X - OldPosition.X);
            float DistDifferY = (Position.Y - OldPosition.Y);

            if (Math.Abs(DistDifferX) > Math.Abs(DistDifferY))
            {
                if (DistDifferX < 0)
                {
                    SpriteEffect = SpriteEffects.FlipHorizontally;

                    Texture.ChangeTexture(Walking);
                    Texture.PlaySequence(WalkingAnim);
                }
                else if (DistDifferX > 0)
                {
                    SpriteEffect = SpriteEffects.None;

                    Texture.ChangeTexture(Walking);
                    Texture.PlaySequence(WalkingAnim);
                }
            }
            else if (Math.Abs(DistDifferY) > Math.Abs(DistDifferX))
            {
                /*
                if (DistDifferY < 0)
                    Texture.PlaySequence();
                else if (DistDifferY > 0)
                    Texture.PlaySequence();*/
            }
            else
            {
                Texture.ChangeTexture(Standing);
                Texture.PlaySequence(StandingAnim);
            }
            
            #endregion

            base.Update(gameTime, playScreen);
        }
    }
}
