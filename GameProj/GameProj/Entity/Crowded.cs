﻿using GameProj.Line;
using GameProj.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameProj.Entity
{
    class Crowded : EntityBase
    {
        private LineBase[] LineBase;

        public Crowded(LineBase[] lineBase)
        {
            Bound.Width = 200;
            Bound.Height = 384;

            Damage = 1;
            Health = 10;
            MaxHealth = 10;
            Mana = 10;
            MaxMana = 10;
            Speed = 2.0f;

            Texture = new TextureAtlas(Main.txtrHolder, 1, 1, 0, 0);

            HoldingItem = null;

            LineBase = lineBase;
        }



        public override void Update(GameTime gameTime, Playing playScreen)
        {
            #region Update Facing Texture
            /*
            float DistDifferX = (Bound.X - Position.X);
            float DistDifferY = (Bound.Y - Position.Y);

            if (Math.Abs(DistDifferX) > Math.Abs(DistDifferY))
            {
                if (DistDifferX < 0)
                    Texture.PlaySequence();
                else if (DistDifferX > 0)
                    Texture.PlaySequence();
            }
            else if (Math.Abs(DistDifferY) > Math.Abs(DistDifferX))
            {
                if (DistDifferY < 0)
                    Texture.PlaySequence();
                else if (DistDifferY > 0)
                    Texture.PlaySequence();
            }
            */
            #endregion

            base.Update(gameTime, playScreen);
        }




        public Vector2[] CustomPos = new Vector2[]
        {
            new Vector2(-5, 0),
            new Vector2(120, 0),
            new Vector2(60, 0),
            new Vector2(160, 0),
            new Vector2(-100, 40),
        };

        public float[] CustomSca = new float[]
        {
            384,
            384,
            384,
            384,
            384,
        };

        public TextureAtlas[] CustomChar = new TextureAtlas[]
        {
            new TextureAtlas(Main.txtrNPC1_Idle, 2, 1, 0, 0),
            new TextureAtlas(Main.txtrNPC2_Idle, 2, 1, 0, 0),
            new TextureAtlas(Main.txtrNPC3_Idle, 2, 1, 0, 0),
            new TextureAtlas(Main.txtrMaria_Idle, 2, 1, 0, 0),
            new TextureAtlas(Main.txtrSoldier_Idle, 8, 1, 0, 0),
        };

        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < CustomChar.Length; i ++)
            {
                #region Draw Sprite

                Vector2 DrawOrigin = Vector2.Zero;
                float DrawScale = 1f;

                #region Origin/ Scale (X)

                DrawOrigin.X = CustomChar[i].DrawOrigin.X;

                #endregion

                #region Origin/ Scale (Y)

                DrawOrigin.Y = CustomChar[i].DrawOrigin.Y;
                DrawScale = (CustomSca[i] / (float)CustomChar[i].SingleHeight);

                #endregion

                Color Color = (HurtTick > 0) ? Color.OrangeRed : Tint;
                float ColorAlpha = (CooldownTick > 0) ? 0.75f : 1.0f;

                spriteBatch.Draw
                    (CustomChar[i].Texture, Position + CustomPos[i], CustomChar[i].CurrView,
                     Color * ColorAlpha, 0, DrawOrigin, DrawScale, SpriteEffects.FlipHorizontally, 0);

                #endregion
            }
        }



        public override void Collided(Player player, Playing playScreen)
        {
            if (LineBase != null)
                playScreen.InitTalkScene(this, LineBase);
        }
    }
}
